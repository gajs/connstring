﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace ConnString
{
    class Program
    {
        private static string connectionString;

        static void Main(string[] args)
        {
#if TEST
            connectionString = ConfigurationManager.ConnectionStrings["TEST"].ConnectionString;
#elif JS
            connectionString = ConfigurationManager.ConnectionStrings["JS"].ConnectionString;
#elif HB
            connectionString = ConfigurationManager.ConnectionStrings["HB"].ConnectionString;
#else
            connectionString = ConfigurationManager.ConnectionStrings["PRODUCTION"].ConnectionString;
#endif
            Console.WriteLine(connectionString);
            Console.ReadKey();
        }
    }
}
